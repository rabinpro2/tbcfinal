package com.tbc.rabinpro.todopro.About;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

//import com.tbc.rabinpro.todopro.Analytics.AnalyticsApplication;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.tbc.rabinpro.todopro.AppDefault.AppDefaultFragment;
import com.tbc.rabinpro.todopro.R;

public class AboutFragment extends AppDefaultFragment {

    private TextView mVersionTextView;
    private String appVersion = "0.1";
    private Toolbar toolbar;
    private TextView logged_in_name;
    private TextView logged_in_email;
    private ImageView imageuser;
    //private AnalyticsApplication app;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //app = (AnalyticsApplication) getActivity().getApplication();
        //app.send(this);
        mVersionTextView = (TextView) view.findViewById(R.id.aboutVersionTextView);
        mVersionTextView.setText(String.format(getResources().getString(R.string.app_version), appVersion));
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        logged_in_name = (TextView) view.findViewById(R.id.logged_in_name);
        logged_in_email = (TextView) view.findViewById(R.id.logged_in_email);
        imageuser = (ImageView) view.findViewById(R.id.image);


        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();
            logged_in_name.setText(personName);
            logged_in_email.setText(personEmail);
            imageuser.setImageURI(personPhoto);
        }
    }

    @LayoutRes
    protected int layoutRes() {
        return R.layout.fragment_about;
    }

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }
}
